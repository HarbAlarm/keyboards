#pragma once

#include "keymap.h"

// clang-format off


#define HA_SECT KC_GRV  // §
#define HA_QUOT KC_MINS // '
#define HA_CIRC KC_EQL  // ^ (dead)

#define HA_DIAE KC_RBRC // ¨ (dead)

#define HA_AE KC_QUOT // ä
#define HA_UE KC_LBRC // ü
#define HA_OE KC_SCLN // ö
#define HA_DLR  KC_NUHS // $


#define HA_LESS KC_NUBS // <

#define HA_COMM KC_COMM // ,
#define HA_DOT  KC_DOT  // .
#define HA_MINS KC_SLSH // -

#define HA_DEG  S(HA_SECT) // °
#define HA_PLUS S(HA_1)    // +
#define HA_DQUO S(HA_2)    // "
#define HA_ASTR S(HA_3)    // *
#define HA_CCED S(HA_4)    // ç
#define HA_PERC S(HA_5)    // %
#define HA_AMPR S(HA_6)    // &
#define HA_SLSH S(HA_7)    // /
#define HA_LPRN S(HA_8)    // (
#define HA_RPRN S(HA_9)    // )
#define HA_EQL  S(HA_0)    // =
#define HA_QUES S(HA_QUOT) // ?
#define HA_GRV  S(HA_CIRC) // ` (dead)
// Row 2
#define HA_EGRV S(HA_UE) // è
#define HA_EXLM S(HA_DIAE) // !
// Row 3
#define HA_EACU S(HA_OE) // é
#define HA_AGRV S(HA_AE) // à
#define HA_PND  S(HA_DLR)  // £
// Row 4
#define HA_MORE S(HA_LESS) // >
#define HA_SCLN S(HA_COMM) // ;
#define HA_COLN S(HA_DOT)  // :
#define HA_UNDS S(HA_MINS) // _

#define HA_BRKP ALGR(HA_1)    // ¦
#define HA_AT   ALGR(HA_2)    // @
#define HA_HASH ALGR(HA_3)    // #
#define HA_NOT  ALGR(HA_6)    // ¬
#define HA_PIPE ALGR(HA_7)    // |
#define HA_CENT ALGR(HA_8)    // ¢
#define HA_ACUT ALGR(HA_QUOT) // ´ (dead)
#define HA_TILD ALGR(HA_CIRC) // ~ (dead)
// Row 2
#define HA_EURO ALGR(HA_E)    // €
#define HA_LBRC ALGR(HA_UE) // [
#define HA_RBRC ALGR(HA_DIAE) // ]
// Row 3
#define HA_LCBR ALGR(HA_ADIA) // {
#define HA_RCBR ALGR(HA_DLR)  // }
// Row 4
#define HA_BSLS ALGR(HA_LESS) // (backslash)

